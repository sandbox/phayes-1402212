<?php
/**
 * @file
 * Bulk export of brick_default objects generated by Bulk export module.
 */

/**
 * Implements hook_default_brick().
 */
function brick_example_default_brick() {
  $bricks = array();

  $brick = new stdClass;
  $brick->disabled = FALSE; /* Edit this to true to make a default brick disabled initially */
  $brick->api_version = 1;
  $brick->name = 'last_user_login';
  $brick->category = 'Example';
  $brick->admin_title = 'Last user login';
  $brick->admin_description = '';
  $brick->cache = 5;
  $brick->content_type = 'custom';
  $brick->subtype = 'custom';
  $brick->conf = array(
    'admin_title' => '',
    'title' => '',
    'body' => '%user:name was last seen at %user:last-login.',
    'format' => 'plain_text',
    'substitute' => TRUE,
    'override_title' => 0,
    'override_title_text' => '',
  );
  $brick->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'context_exists',
        'settings' => array(
          'exists' => '1',
        ),
        'context' => 'argument_entity_id:user_1',
        'not' => FALSE,
      ),
      1 => array(
        'name' => 'path_visibility',
        'settings' => array(
          'visibility_setting' => '1',
          'paths' => 'user*',
        ),
        'context' => 'empty',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $brick->requiredcontexts = array(
    0 => array(
      'identifier' => 'User',
      'keyword' => 'user',
      'name' => 'user',
      'id' => 1,
    ),
  );
  $brick->contexts = array();
  $brick->relationships = array();
  $bricks['last_user_login'] = $brick;

  $brick = new stdClass;
  $brick->disabled = FALSE; /* Edit this to true to make a default brick disabled initially */
  $brick->api_version = 1;
  $brick->name = 'search_users';
  $brick->category = 'Example';
  $brick->admin_title = 'Search users';
  $brick->admin_description = 'Search users as the same time as content.';
  $brick->cache = -1;
  $brick->content_type = 'search_result';
  $brick->subtype = 'search_result';
  $brick->conf = array(
    'type' => 'user',
    'log' => TRUE,
    'override_empty' => TRUE,
    'empty_title' => '',
    'empty' => 'No users match your search.',
    'empty_format' => 'plain_text',
    'override_no_key' => FALSE,
    'no_key_title' => '',
    'no_key' => '',
    'no_key_format' => 'plain_text',
    'context' => 'requiredcontext_string_1',
    'override_title' => 1,
    'override_title_text' => 'Users with %string',
  );
  $brick->access = array(
    'logic' => 'and',
    'plugins' => array(
      0 => array(
        'name' => 'path_visibility',
        'settings' => array(
          'visibility_setting' => '1',
          'paths' => 'search/node*',
        ),
        'context' => 'empty',
        'not' => FALSE,
      ),
    ),
  );
  $brick->requiredcontexts = array(
    0 => array(
      'identifier' => 'String',
      'keyword' => 'string',
      'name' => 'string',
      'id' => 1,
    ),
  );
  $brick->contexts = array();
  $brick->relationships = array();
  $bricks['search_users'] = $brick;

  $brick = new stdClass;
  $brick->disabled = FALSE; /* Edit this to true to make a default brick disabled initially */
  $brick->api_version = 1;
  $brick->name = 'sidebar_image';
  $brick->category = 'Example';
  $brick->admin_title = 'Sidebar image';
  $brick->admin_description = 'Show the image field from the current node in the sidebar.';
  $brick->cache = 5;
  $brick->content_type = 'entity_field';
  $brick->subtype = 'node:field_image';
  $brick->conf = array(
    'label' => 'title',
    'formatter' => 'image',
    'formatter_settings' => array(
      'image_style' => 'thumbnail',
      'image_link' => 'file',
    ),
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 1,
    'override_title_text' => 'Image by %node:author',
  );
  $brick->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'context_exists',
        'settings' => array(
          'exists' => '1',
        ),
        'context' => 'argument_entity_id:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $brick->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'id' => 1,
    ),
  );
  $brick->contexts = array();
  $brick->relationships = array();
  $bricks['sidebar_image'] = $brick;

  $brick = new stdClass;
  $brick->disabled = FALSE; /* Edit this to true to make a default brick disabled initially */
  $brick->api_version = 1;
  $brick->name = 'tag_descriptions';
  $brick->category = 'Example';
  $brick->admin_title = 'tag descriptions';
  $brick->admin_description = 'Shows descriptions for tags on the current node. This uses a view content pane that takes a contextual filter that is a string of all the term IDs selected for this node. The brick\'s context and relationships handle the creation of that argument string.';
  $brick->cache = 5;
  $brick->content_type = 'views_panes';
  $brick->subtype = 'tag_descriptions-panel_pane_1';
  $brick->conf = array(
    'context' => array(
      0 => 'relationship_terms_from_node_1',
    ),
  );
  $brick->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'context_exists',
        'settings' => array(
          'exists' => '1',
        ),
        'context' => 'requiredcontext_entity:node_1',
        'not' => FALSE,
      ),
    ),
    'logic' => 'and',
  );
  $brick->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'id' => 1,
    ),
  );
  $brick->contexts = array();
  $brick->relationships = array(
    0 => array(
      'identifier' => 'Multiple terms from node',
      'keyword' => 'terms',
      'name' => 'terms_from_node',
      'context' => 'requiredcontext_entity:node_1',
      'vid' => array(
        1 => '1',
      ),
      'concatenator' => ',',
      'id' => 1,
    ),
  );
  $bricks['tag_descriptions'] = $brick;

  $brick = new stdClass;
  $brick->disabled = FALSE; /* Edit this to true to make a default brick disabled initially */
  $brick->api_version = 1;
  $brick->name = 'user_contact_form';
  $brick->category = 'Example';
  $brick->admin_title = 'User contact form';
  $brick->admin_description = 'Adds the user contact form to the user\'s profile page.';
  $brick->cache = 5;
  $brick->content_type = 'user_contact';
  $brick->subtype = 'user_contact';
  $brick->conf = array(
    'context' => 'requiredcontext_user_1',
    'override_title' => 1,
    'override_title_text' => 'Get in touch with %user:name',
  );
  $brick->access = array(
    'plugins' => array(),
    'logic' => 'and',
  );
  $brick->requiredcontexts = array(
    0 => array(
      'identifier' => 'User',
      'keyword' => 'user',
      'name' => 'user',
      'id' => 1,
    ),
  );
  $brick->contexts = array();
  $brick->relationships = array();
  $bricks['user_contact_form'] = $brick;

  return $bricks;
}
