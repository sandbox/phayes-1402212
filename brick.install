<?php

/**
 * Implementation of hook_schema().
 */
function brick_schema() {
  // This should always point to our 'current' schema. This makes it relatively easy
  // to keep a record of schema as we make changes to it.
  return brick_schema_1();
}

/**
 * Schema version 1 for Panels in D6.
 */
function brick_schema_1() {
  $schema = array();

  $schema['brick'] = array(
    'export' => array(
      'identifier' => 'brick',
      'delete callback' => 'brick_delete',
      'api' => array(
        'owner' => 'brick',
        'api' => 'brick_default',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'name' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'description' => "The block's {block}.delta.",
      ),
      'category' => array(
        'type' => 'varchar',
        'length' => '64',
        'default' => 'Brick',
        'description' => 'The category of this brick on block admin page.',
      ),
      'admin_title' => array(
        'type' => 'varchar',
        'length' => '128',
        'description' => 'The administrative title of the brick.',
      ),
      'admin_description' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Administrative title of this brick.',
        'object default' => '',
      ),
      'cache' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => DRUPAL_NO_CACHE,
        'size' => 'small',
        'description' => 'Binary flag to indicate block cache mode. (-2: Custom cache, -1: Do not cache, 1: Cache per role, 2: Cache per user, 4: Cache per page, 8: Block cache global) See DRUPAL_CACHE_* constants in ../includes/common.inc for more detailed information.',
      ),
      'content_type' => array(
        'type' => 'varchar',
        'length' => '32',
        'default' => 'custom',
      ),
      'subtype' => array(
        'type' => 'varchar',
        'length' => '64',
        'default' => 'custom',
      ),
      'conf' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'object default' => array(),
        'initial ' => array(),
      ),
      'access' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'object default' => array(),
        'description' => 'An array of access settings.',
      ),
      'requiredcontexts' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'object default' => array(),
        'description' => 'An array of required contexts.',
      ),
      'contexts' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'object default' => array(),
        'description' => 'An array of contexts embedded into the brick.',
      ),
      'relationships' => array(
        'type' => 'text',
        'size' => 'big',
        'serialize' => TRUE,
        'object default' => array(),
        'description' => 'An array of relationships embedded into the brick.',
      ),
    ),
    'primary key' => array('name'),
  );

  return $schema;
}

/**
 * Implementation of hook_uninstall().
 */
function brick_uninstall() {

  $result = db_query("SELECT * FROM {brick}");
  $names = array();
  foreach ($result as $brick) {
    $names[] = $brick->name;
  }

  if ($names) {
    // Delete all configured blocks.
    db_delete('block')
      ->condition('module', 'brick')
      ->condition('name', $names)
      ->execute();
  }
}
